package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BMIScreen {

    private AppiumDriver<MobileElement> driver;

    private By input_height = MobileBy.AccessibilityId("");
    private By input_weight = MobileBy.AccessibilityId("");
    private By btn_metric = MobileBy.AccessibilityId("");
    private By btn_imperial = By.xpath(""); // Skip this one for now, will be explained later
    private By btn_info = MobileBy.AccessibilityId("");
    private By lbl_bmiResult = MobileBy.AccessibilityId("");
    private By lbl_bmiDescription = MobileBy.AccessibilityId("");

    public BMIScreen(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
    }

    public void enterHeight(String height) {
        // Write some code to enter a height
    }

    public void enterWeight(String weight) {
        // Write some code to enter a weight
    }

    public void clickMetric() {
        driver.hideKeyboard(); // On small viewports, the keyboard hides the Metric button, leave this line in here!

        // Write some code to click the metric radio button
    }
}
