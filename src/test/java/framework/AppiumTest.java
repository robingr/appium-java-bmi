package framework;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.BMIScreen;

import java.io.IOException;
import java.net.URL;

public class AppiumTest {

    private static AppiumDriver<MobileElement> driver;
    protected BMIScreen bmiScreen;

    @BeforeAll
    static void setUp() throws IOException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("automationName", "");
        capabilities.setCapability("deviceName", "");
        capabilities.setCapability("appPackage", "");
        capabilities.setCapability("appActivity", "");

        driver = new AndroidDriver<>(new URL(""), capabilities);
    }

    @BeforeEach
    void testSetUp() {
        bmiScreen = new BMIScreen(driver);
    }

    @AfterEach
    void testTearDown() {

    }

    @AfterAll
    static void tearDown() {
        driver.quit();
    }

}
