## appium-java-training

This is a project for the course 'Mobile App automation using Appium in Java'.
It tests the BMI calculator app found in apps/ by performing a simple calculation.

## First use:
0. Import the project in IntelliJ IDEA or a different IDE if necessary.
0. Open a terminal (inside the IDE or outside it) and navigate to the project directory (if not already there).
1. Install the app on the preferred device with
`adb install apps/bmi_calculator.apk`.
2. Fill in all capabilities in `AppiumTest.java`.
3. Start the Appium server.
4. Start the test by clicking the play-button next to `give_this_test_a_better_name` in `BMICalcTest`.
Alternatively, you can use the `mvn test` command in the terminal.
5. The test should start the BMI calculator on the connected device. The test is allowed to fail if the locators in BMIScreen.java have not been filled in yet.
